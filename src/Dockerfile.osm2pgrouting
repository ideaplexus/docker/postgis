ARG POSTGRES_VERSION={{ index .Postgres 0 }}
ARG BUILD_CACHE_PREFIX
ARG PROXY_CACHE_PREFIX

FROM ${BUILD_CACHE_PREFIX}postgis-libpqxx:postgres-${POSTGRES_VERSION} AS libpqxx
FROM ${PROXY_CACHE_PREFIX}postgres:${POSTGRES_VERSION}-alpine{{ .Alpine }} AS postgres

FROM ${PROXY_CACHE_PREFIX}alpine:{{ .Alpine }}

ENV OSM2PGROUTING_VERSION={{ .Sources.osm2pgrouting.Version }}
ENV OSM2PGROUTING_SHA256={{ .Sources.osm2pgrouting.SHA256Sum }}

COPY --from=libpqxx /usr/local /usr/local
COPY --from=postgres /usr/local /usr/local

RUN set -eux \
    && apk upgrade --update-cache --no-cache --ignore alpine-baselayout \
    && apk add --no-cache --virtual .fetch-deps \
        ca-certificates \
        openssl \
        tar \
    && mkdir -p /usr/src \
    && cd /usr/src \
    && wget -q -O osm2pgrouting.tar.gz "https://github.com/pgRouting/osm2pgrouting/archive/v$OSM2PGROUTING_VERSION.tar.gz" \
    && sha256sum osm2pgrouting.tar.gz \
    && echo "$OSM2PGROUTING_SHA256 *osm2pgrouting.tar.gz" | sha256sum -c - \
    && mkdir -p /usr/src/osm2pgrouting \
    && tar \
        --extract \
        --file osm2pgrouting.tar.gz \
        --directory /usr/src/osm2pgrouting \
        --strip-components 1
RUN set -eux \
    && apk upgrade --update-cache --no-cache --ignore alpine-baselayout \
    && apk add --no-cache --virtual .build-deps \
        build-base \
        cmake \
        ninja \
        expat-dev boost-dev \
    && apk add --no-cache --virtual .peer-deps \
        openldap-dev krb5-dev \
    && cd /usr/src/osm2pgrouting \
    && sed -i 's/#include "osm_elements\/osm_tag.h"/#include "osm_elements\/osm_tag.h"\n#include <algorithm>/g' 'src/osm_elements/osm_tag.cpp' \
    && cmake -B build -G Ninja \
    && cmake --build build \
    && cmake --install build \
    && cd /usr/src \
    && rm -rf /usr/src/osm2pgrouting \
    && mkdir -p /usr/src/osm2pgrouting \
    && tar \
        --extract \
        --file osm2pgrouting.tar.gz \
        --directory /usr/src/osm2pgrouting \
        --strip-components 1 \
    && rm -rf osm2pgrouting.tar.gz \
    && apk del .fetch-deps .build-deps .peer-deps
