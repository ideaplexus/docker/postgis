ARG POSTGRES_VERSION=17
ARG BUILD_CACHE_PREFIX
ARG PROXY_CACHE_PREFIX

FROM ${BUILD_CACHE_PREFIX}postgis-proj AS proj
FROM ${BUILD_CACHE_PREFIX}postgis-geos AS geos
FROM ${BUILD_CACHE_PREFIX}postgis-cgal AS cgal
FROM ${BUILD_CACHE_PREFIX}postgis-sfcgal AS sfcgal
FROM ${PROXY_CACHE_PREFIX}postgres:${POSTGRES_VERSION}-alpine3.21 AS postgres

FROM ${PROXY_CACHE_PREFIX}alpine:3.21

ENV GDAL_VERSION=3.10.1
ENV GDAL_SHA256=e630367e633eafbc46bca427ca7b1e7b7264ad01fd62cbff460ee1539720b60a

COPY --from=proj /usr/local /usr/local
COPY --from=geos /usr/local /usr/local
COPY --from=cgal /usr/local /usr/local
COPY --from=sfcgal /usr/local /usr/local
COPY --from=postgres /usr/local /usr/local

RUN set -eux \
    && apk upgrade --update-cache --no-cache --ignore alpine-baselayout \
    && apk add --no-cache --virtual .fetch-deps \
        ca-certificates \
        openssl \
        tar \
    && mkdir -p /usr/src \
    && cd /usr/src \
    && wget -q -O gdal.tar.gz "https://github.com/OSGeo/gdal/archive/v$GDAL_VERSION.tar.gz" \
    && sha256sum gdal.tar.gz \
    && echo "$GDAL_SHA256 *gdal.tar.gz" | sha256sum -c - \
    && mkdir -p /usr/src/gdal \
    && tar \
        --extract \
        --file gdal.tar.gz \
        --directory /usr/src/gdal \
        --strip-components 1 \
    && apk add --no-cache --virtual .build-deps \
        build-base \
        cmake \
        ninja \
        apache-ant cfitsio-dev chrpath armadillo-dev curl-dev pcre2-dev expat-dev bison doxygen blosc-dev libjxl-dev libdeflate-dev freexl-dev giflib-dev opencl-dev qhull-dev lz4-dev hdf5-dev json-c-dev krb5-libs libgeotiff-dev libspatialite-dev libheif-dev libkml-dev libpng-dev libwebp-dev libxml2-dev linux-headers netcdf-dev openexr-dev openjdk11 openjpeg-dev openldap-dev poppler-dev py3-numpy py3-numpy-dev py3-setuptools python3-dev sqlite-dev kealib-dev swig unixodbc-dev xerces-c-dev zlib-dev zstd-dev libaec-dev \
    && apk add --no-cache --virtual .sfcgal-build-deps \
        mpfr-dev gmp-dev boost-dev \
    && cd /usr/src/gdal \
    && cmake -B build -G Ninja -DGDAL_USE_OPENCL=ON \
    && cmake --build build \
    && cmake --install build \
    && cd /usr/src \
    && rm -rf /usr/src/gdal \
    && mkdir -p /usr/src/gdal \
    && tar \
        --extract \
        --file gdal.tar.gz \
        --directory /usr/src/gdal \
        --strip-components 1 \
    && rm gdal.tar.gz \
    && apk del .fetch-deps .build-deps .sfcgal-build-deps
