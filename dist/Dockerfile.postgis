ARG POSTGRES_VERSION=17
ARG BUILD_CACHE_PREFIX
ARG PROXY_CACHE_PREFIX

FROM ${BUILD_CACHE_PREFIX}postgis-proj AS proj
FROM ${BUILD_CACHE_PREFIX}postgis-geos AS geos
FROM ${BUILD_CACHE_PREFIX}postgis-cgal AS cgal
FROM ${BUILD_CACHE_PREFIX}postgis-sfcgal AS sfcgal
FROM ${BUILD_CACHE_PREFIX}postgis-gdal:postgres-${POSTGRES_VERSION} AS gdal
FROM ${PROXY_CACHE_PREFIX}postgres:${POSTGRES_VERSION}-alpine3.21 AS postgres

FROM ${PROXY_CACHE_PREFIX}alpine:3.21

ENV POSTGIS_VERSION=3.5.2
ENV POSTGIS_SHA256=71d8f5d06debec0bdd94525a5a1d42417ed2a1f65d0070a1a3e3e381b606ee35

COPY --from=proj /usr/local /usr/local
COPY --from=geos /usr/local /usr/local
COPY --from=cgal /usr/local /usr/local
COPY --from=sfcgal /usr/local /usr/local
COPY --from=gdal /usr/local /usr/local
COPY --from=postgres /usr/local /usr/local

RUN set -eux \
    && apk upgrade --update-cache --no-cache --ignore alpine-baselayout \
    && apk add --no-cache --virtual .fetch-deps \
        ca-certificates \
        openssl \
        tar \
    && mkdir -p /usr/src \
    && cd /usr/src \
    && wget -q -O postgis.tar.gz "https://github.com/postgis/postgis/archive/$POSTGIS_VERSION.tar.gz" \
    && sha256sum postgis.tar.gz \
    && echo "$POSTGIS_SHA256 *postgis.tar.gz" | sha256sum -c - \
    && mkdir -p /usr/src/postgis \
    && tar \
      --extract \
      --file postgis.tar.gz \
      --directory /usr/src/postgis \
      --strip-components 1 \
    && apk add --no-cache --virtual .build-deps \
      autoconf automake libtool build-base libxml2-dev json-c-dev cunit-dev protobuf-c-dev pcre2-dev git clang19 llvm19 \
    && PEER_DEPS="$( \
        scanelf --needed --nobanner --format '%n#p' /usr/local/bin/* /usr/local/lib/* /usr/local/lib/postgresql/* \
            | tr ',' '\n' \
            | sort -u \
            | awk 'system("[ -e /usr/local/lib/" $1 " ] ") == 0 { next } { print "so:" $1 }' \
    )" \
    && apk add --no-cache --virtual .peer-deps \
      $PEER_DEPS \
    && cd /usr/src/postgis \
    && ./autogen.sh \
    && ./configure \
      CXXFLAGS=-I/usr/local/include \
      LDFLAGS=-L/usr/local/lib \
    && make -j "$(nproc)" \
    && make install \
    && cd /usr/src \
    && rm -rf /usr/src/postgis \
    && mkdir -p /usr/src/postgis \
    && tar \
      --extract \
      --file postgis.tar.gz \
      --directory /usr/src/postgis \
      --strip-components 1 \
    && rm postgis.tar.gz \
    && apk del .fetch-deps .build-deps .peer-deps
