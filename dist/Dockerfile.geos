ARG PROXY_CACHE_PREFIX

FROM ${PROXY_CACHE_PREFIX}alpine:3.21

ENV GEOS_VERSION=3.13.0
ENV GEOS_SHA256=351375d3697000d94a6b3d4041f08e12221f4eb065ed412c677960a869518631

RUN set -eux \
    && apk upgrade --update-cache --no-cache --ignore alpine-baselayout \
    && apk add --no-cache --virtual .fetch-deps \
        ca-certificates \
        openssl \
        tar \
    && mkdir -p /usr/src \
    && cd /usr/src \
    && wget -q -O geos.tar.gz "https://github.com/libgeos/geos/archive/$GEOS_VERSION.tar.gz" \
    && sha256sum geos.tar.gz \
    && echo "$GEOS_SHA256 *geos.tar.gz" | sha256sum -c - \
    && mkdir -p /usr/src/geos \
    && tar \
        --extract \
        --file geos.tar.gz \
        --directory /usr/src/geos \
        --strip-components 1 \
    && apk add --no-cache --virtual .build-deps \
        build-base \
        cmake \
        ninja \
    && cd /usr/src/geos \
    && cmake -B build -G Ninja -DCMAKE_BUILD_TYPE=Release \
    && cmake --build build \
    && cmake --install build \
    && cd /usr/src \
    && rm -rf /usr/src/geos \
    && mkdir -p /usr/src/geos \
    && tar \
        --extract \
        --file geos.tar.gz \
        --directory /usr/src/geos \
        --strip-components 1 \
    && rm geos.tar.gz \
    && apk del .fetch-deps .build-deps
